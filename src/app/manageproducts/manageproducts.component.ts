import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manageproducts',
  templateUrl: './manageproducts.component.html',
  styleUrls: ['./manageproducts.component.css']
})
export class ManageproductsComponent implements OnInit {
  requestURL:string="http://localhost:3000/webapiadmin/manageproducts"
  requestURL1:string="http://localhost:3000/webapiadmin/fetchproducts"
  requestURL2:string="http://localhost:3000/webapiadmin/fetchadmincategory"
  requestURL3:string="http://localhost:3000/webapiadmin/uploadpic"

  constructor(private _http:HttpClient,private _route:Router, ) { }
  prodlist:any;
  catelist:any;
  private fileToUpload : any = null;
  plist:any
  ngOnInit(): void {
    this._http.get(this.requestURL1).subscribe((data)=>{
      this.prodlist=data;
    })
    this._http.get(this.requestURL2).subscribe((data)=>{
      this.catelist=data;
    })

    this._http.get(this.requestURL3).subscribe((response:any)=>
    {
      this.plist = response;
    });
    
  }

  onClickSubmit(details:any)
  { 
    this._http.post(this.requestURL,details).subscribe((data)=>{
      this.showResponse(data)  
      this.ngOnInit()                          
    })

  }

  showResponse(data:any)
  {
    if(data.response=='success') {
      alert('product added successfully')
      this._route.navigate(['/manageproducts'])
    }
    else
     alert('failed to add product');   
  }

  handleChangePic(evt:Event) 
  {
    var files:any = (<HTMLInputElement>evt.target).files;
    //files: FileList
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload)
  }

  upload(pid:string)
  {   
    //alert(pid)
    if(this.fileToUpload==null)
      alert('Please Select Image First !');
    else 
    {  
      const formData: FormData = new FormData();
      formData.append('product_image', this.fileToUpload, this.fileToUpload.name);
      formData.append('pid',pid);    

      this._http.post(this.requestURL3, formData).subscribe((response:any)=>
      {
          if(response.status)
          {
            console.log("IT is working")
            this.ngOnInit()
          }
      });
      this.fileToUpload = null;
    }
  }

 

}
