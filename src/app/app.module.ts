import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RequesturlService } from './requesturl.service';
import { AuthGuard } from './auth.guard';
import { UserauthGuard } from './userauth.guard';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { ProductComponent } from './product/product.component';
import { HomefooterComponent } from './homefooter/homefooter.component';
import { HomeheaderComponent } from './homeheader/homeheader.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { UserhomeComponent } from './userhome/userhome.component';
import { ManageusersComponent } from './manageusers/manageusers.component';
import { AuthService } from './auth.service';
import { MenuComponent } from './menu/menu.component';
import { CpadminComponent } from './cpadmin/cpadmin.component';
import { CpuserComponent } from './cpuser/cpuser.component';
import { EpadminComponent } from './epadmin/epadmin.component';
import { EpuserComponent } from './epuser/epuser.component';
import { ManageproductsComponent } from './manageproducts/manageproducts.component';
import { AdmincategoryComponent } from './admincategory/admincategory.component';
import { CartComponent } from './cart/cart.component';
import { BuyproductComponent } from './buyproduct/buyproduct.component';
import { PaymentComponent } from './payment/payment.component';
import { CodComponent } from './cod/cod.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ContactComponent,
    ProductComponent,
    HomefooterComponent,
    HomeheaderComponent,
    LoginComponent,
    RegisterComponent,
    AdminhomeComponent,
    UserhomeComponent,
    ManageusersComponent,
    MenuComponent,
    CpadminComponent,
    CpuserComponent,
    EpadminComponent,
    EpuserComponent,
    ManageproductsComponent,
    AdmincategoryComponent,
    CartComponent,
    BuyproductComponent,
    PaymentComponent,
    CodComponent,

 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [RequesturlService,AuthGuard,AuthService,UserauthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
