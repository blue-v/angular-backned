import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  requestURL:string="http://localhost:3000/webapiuser/fetchProdDetails"
  constructor(private _http:HttpClient,private _authService:AuthService,private _route:Router) { }
  prodDetails:any
  name:any
  pDetails:any
  _id:any
  public cart:any=[]
  ngOnInit(): void {
    
	  this._http.get(this.requestURL).subscribe((data:any)=>{
		  //console.log(data)
      this.prodDetails=data
	  })
  }
  //To show Add to cart button only on user panel after login
  checkTokenUser(){
    var res = this._authService.getTokenUser()
    if(res)
      this.name = localStorage.getItem('name')
    return res
	}
  addtocart(prodimage:any,prodtitle:any,prodprice:any){
    alert("Product added to Cart Successfully")
    this._id=localStorage.getItem('_id')
    if(!!localStorage.getItem('cart'))
    {
      var data=JSON.parse(localStorage.getItem('cart')!)
      this.cart=data
      this.cart.push({'ProductImage':prodimage,'ProductTitle':prodtitle,'ProductPrice':prodprice,'_id':this._id})
      localStorage.setItem('cart',JSON.stringify(this.cart))
    }else{
      this.cart.push({'ProductImage':prodimage,'ProductTitle':prodtitle,'ProductPrice':prodprice,'_id':this._id})
      localStorage.setItem('cart',JSON.stringify(this.cart))

    }
  }

}
