var server = "http://localhost:3000";

export const ADMIN_LOGIN =  server + "/admin/login";
export const ADMIN_CATEGORY =  server + "/admin/category";
export const ADMIN_PRODUCT =  server + "/admin/product";
export const PRODUCT_UPLOAD_PIC =  server + "/admin/uploadpic";
export const SEARCH_PRODUCT =  server + "/admin/searchproduct";

export const ADD_CART =  server + "/admin/addcart";
export const LOAD_CART =  server + "/admin/loadcart";
export const GET_PRODUCT =  server + "/admin/getproduct";

export const USER_REGISTER =  server + "/user/register";
export const USER_LOGIN =  server + "/user/login";
export const USER_PAYMENT =  server + "/user/pay";

export const CHECK_SESSION =  server + "/checksession";
export const LOGOUT =  server + "/logout";