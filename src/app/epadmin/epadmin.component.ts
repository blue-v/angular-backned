import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RequesturlService } from '../requesturl.service';

@Component({
  selector: 'app-epadmin',
  templateUrl: './epadmin.component.html',
  styleUrls: ['./epadmin.component.css']
})
export class EpadminComponent implements OnInit {

  constructor(private _http:HttpClient , private _requestURL:RequesturlService , private _router:Router) { }
  name:any
  email:any
  address:any
  city:any
  gender:any
  phone:any
  
  ngOnInit(): void {
    var requestURL=this._requestURL.requestURL_admin+'epadmin'  
    this._http.post(requestURL,{'email':localStorage.getItem('email')}).subscribe((data:any)=>{
      
      this.name=data.name
      this.email=data.email
      this.phone=data.phone
      this.address=data.address
      this.city=data.city
      if(data.gender=="male")
        this.gender="male"
      else
        this.gender="female"
    })
  }

  onClickSubmit(userDetails:any)
  {
    var requestURL=this._requestURL.requestURL_admin+'upradmin'      
    this._http.post(requestURL,userDetails).subscribe((data)=>{
      alert("Admin profile updated successfully.....")
      //this._router.navigate([''])  
      this.ngOnInit()
    })
  }

}