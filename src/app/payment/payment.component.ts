import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequesturlService } from '../requesturl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  userId: any
  buyall: any
  userhistorylist: any
  history:any
  totalamount:number=0
  price:any
  paymentHandler:any = null;
  pDetails:any

  constructor(private route:Router, private _requestURL:RequesturlService, private _http:HttpClient) { }

  requestURL:string=this._requestURL.requestURL_user+'payment'

  ngOnInit(): void {
    this.buyall = []
    this.history=[]
    this.invokeStripe();

    
    if (!!localStorage.getItem('buyall')) {
      var data = JSON.parse(localStorage.getItem('buyall')!)
      this.buyall = data
      this.buyall.forEach((item: any, index: any) => { 

        if (item._id === this.userId) {
          this.buyall.push(item)
          
        }
        
        //console.log(typeof(this.price))
        //console.log(this.totalamount)
        this.price=parseInt(item.ProductPrice)
        this.totalamount = this.totalamount + this.price
        localStorage.setItem('totalamount',this.totalamount.toString())
        
      })
      
    }
    /*if (!!localStorage.getItem('buy')) {
      var data = JSON.parse(localStorage.getItem('buy')!)
      this.buyall = data
      this.buyall.forEach((item: any, index: any) => { //to match _id with user_id and show products for that user only
        if (item._id === this.userId) {
          this.buyall.push(item)
        }
      })
    }*/
  }

  makePaymentTest(amount:any){

    
    if(!!localStorage.getItem('history'))
    {
        var data=JSON.parse(localStorage.getItem('history')!)
        //this.history=data
        for(let i of this.buyall){
            this.history.push(i)
        }
        localStorage.setItem('history',JSON.stringify(this.history))
        
    }else{
        for(let i of this.buyall){
          this.history.push(i)
        }
        localStorage.setItem('history',JSON.stringify(this.history))
        
        
    }
    //end add to history in localstorage

    //make payment
    this.makePayment(amount,(txnid:any)=>{

      var pDetails = {'username':localStorage.getItem('username'),'txnid':txnid,'amount':amount,'prodDetails':this.buyall,'info':new Date()}
    	this._http.post(this.requestURL,pDetails).subscribe((data:any)=>{
        console.log(data)
        alert('payment successful')
        this.route.navigate(['/buyproduct/invoice'])
      })
    })
  }

  makePayment(amount:any,cb:any) {
    const paymentHandler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_51Jc2tTSGeXHJcOnL8e5uoDPjqT4dmxIu2fZZLX3e3D3GByRN13G7fjPMd3kUOIZ4E1YYHrcT3j9sgD6C86hKFNE300fB6Qz68a',
      locale: 'auto',
      token: function (stripeToken: any) {
        //console.log(stripeToken)
        //alert('Payment Successfull....');
        cb(stripeToken.id)
      }
    });
  
    paymentHandler.open({
      name: 'Positronx',
      description: '3 widgets',
      amount: amount * 100
    });
  }

  invokeStripe() {
    if(!window.document.getElementById('stripe-script')) {
      const script = window.document.createElement("script");
      script.id = "stripe-script";
      script.type = "text/javascript";
      script.src = "https://checkout.stripe.com/checkout.js";
      script.onload = () => {
        this.paymentHandler = (<any>window).StripeCheckout.configure({
          key: 'pk_test_51H7bbSE2RcKvfXD4DZhu',
          locale: 'auto',
          token: function (stripeToken: any) {
            console.log(stripeToken)
            alert('Payment has been successfull! The Order is Confirmed. Thank you!!!');
           
          }
        });
      }
        
      window.document.body.appendChild(script);
    }
  }
}