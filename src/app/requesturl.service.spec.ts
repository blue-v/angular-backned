import { TestBed } from '@angular/core/testing';

import { RequesturlService } from './requesturl.service';

describe('RequesturlService', () => {
  let service: RequesturlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RequesturlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
