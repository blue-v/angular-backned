import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { UserauthGuard } from './userauth.guard';


import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { ProductComponent } from './product/product.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserhomeComponent } from './userhome/userhome.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { ManageusersComponent } from './manageusers/manageusers.component';
import { CpadminComponent } from './cpadmin/cpadmin.component';
import { CpuserComponent } from './cpuser/cpuser.component';
import { EpadminComponent } from './epadmin/epadmin.component';
import { EpuserComponent } from './epuser/epuser.component';
import { ManageproductsComponent } from './manageproducts/manageproducts.component';
import { AdmincategoryComponent } from './admincategory/admincategory.component';
import { CartComponent } from './cart/cart.component';
import { BuyproductComponent } from './buyproduct/buyproduct.component';
import { PaymentComponent } from './payment/payment.component';
import { CodComponent } from './cod/cod.component';


const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },

  {
    path:'about',
    component:AboutComponent
  },
  {
    path:'contact',
    component:ContactComponent
  },
  {
    path:'product',
    component:ProductComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  {
    path : 'userhome',
    component : UserhomeComponent,
    canActivate : [UserauthGuard]
  },
  {
    path : 'adminhome',
    component : AdminhomeComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'manageusers',
    component : ManageusersComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'cpadmin',
    component : CpadminComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'epadmin',
    component : EpadminComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'cpuser',
    component : CpuserComponent,
    canActivate : [UserauthGuard]
  },
  {
    path : 'epuser',
    component : EpuserComponent,
    canActivate : [UserauthGuard]
  },
  {
    path:'manageproducts',
    component: ManageproductsComponent,
    canActivate : [AuthGuard]
  },
  {
    path:'admincategory',
    component: AdmincategoryComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'cart',
    component : CartComponent,
    canActivate : [UserauthGuard]
  },
  {
    path:'buyproduct',
    component:BuyproductComponent,
    canActivate:[UserauthGuard],
    children:[
      {path:'payment',component:PaymentComponent},
      {path:'cod',component:CodComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }