import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  title = 'myproject';
  username:any  
  constructor(private _authService:AuthService , private _route:Router){}
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  checkToken()
  {
    return this._authService.getToken()  
  }

  checkTokenAdmin()
  {
    var res=this._authService.getTokenAdmin()
    if(res)
      this.username=localStorage.getItem("username")
    return res  
  }

  checkTokenUser()
  {
    var res=this._authService.getTokenUser()
    if(res)
      this.username=localStorage.getItem("username")
    return res
  }

  logout()
  {
    localStorage.removeItem('token')
    localStorage.removeItem('_id')
    localStorage.removeItem('name')
    localStorage.removeItem('username')
    localStorage.removeItem('address')
    localStorage.removeItem('city')
    localStorage.removeItem('gender')
    localStorage.removeItem('role')
    localStorage.removeItem('info')
    this._route.navigate(['/login'])      
  }


}
