import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http' 
import { Router } from '@angular/router'

@Component({
  selector: 'app-admincategory',
  templateUrl: './admincategory.component.html',
  styleUrls: ['./admincategory.component.css']
})
export class AdmincategoryComponent implements OnInit {
  requestURL:string="http://localhost:3000/webapiadmin/admincategory"
  requestURL1:string="http://localhost:3000/webapiadmin/fetchadmincategory"
 
  responseData:any
  catelist:any;
  constructor(private _http:HttpClient,private _route:Router) { }

  ngOnInit(): void {
    this._http.get(this.requestURL1).subscribe((data)=>{
      this.catelist=data;
    })
  }

  onClickSubmit(details:any)
  {
    this._http.post(this.requestURL,details).subscribe((data)=>{
      this.showResponse(data)    
      this.ngOnInit()                        
    })
  }

  showResponse(data:any)
  {
    if(data.response=='success') 
    {
      alert('successfully add category'); 
      this._route.navigate(['/admincategory']) 
    }
    else 
    alert('failed to add category');   
  }

}
