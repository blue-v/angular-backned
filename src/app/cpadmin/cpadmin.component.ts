import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequesturlService } from '../requesturl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cpadmin',
  templateUrl: './cpadmin.component.html',
  styleUrls: ['./cpadmin.component.css']
})
export class CpadminComponent implements OnInit {

  constructor(private _http:HttpClient , private _requestURL:RequesturlService , private _router:Router) { }

  msg:any

  ngOnInit(): void {
  }

  onClickSubmit(cpdata:any)
  {
    cpdata.email=localStorage.getItem("email")
    console.log(cpdata)
    var requestURL=this._requestURL.requestURL_admin+'cpadmin'
    this._http.post(requestURL,cpdata).subscribe((data:any)=>{
      if(data.response==0)
        this.msg="Invalid Old Password"
      else if(data.response==1)
        this.msg="New & Confirm New Password Does Not Matched"    
      else
      {
        alert("Password Changed Successfully ,Please Login Again")
        localStorage.removeItem("token")      
        this._router.navigate(['/login'])  
      }    
    })
  }
}
