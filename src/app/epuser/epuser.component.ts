import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RequesturlService } from '../requesturl.service';

@Component({
  selector: 'app-epuser',
  templateUrl: './epuser.component.html',
  styleUrls: ['./epuser.component.css']
})
export class EpuserComponent implements OnInit {

  userDetails:any
  name:any
  email:any
  address:any
  city:any
  gender:any
  phone:any
  constructor(private _route:Router,private _http:HttpClient,private _requestURL:RequesturlService) { }

  ngOnInit(): void {
    var requestURL=this._requestURL.requestURL_user+'epuser'
	  this._http.post(requestURL,{'email':localStorage.getItem('email')}).subscribe((data:any)=>{
		  //console.log(data)
      
     
      this.name=data.name
      this.email=data.email
      this.phone=data.phone
      this.address=data.address
      this.city=data.city
      if(data.gender=="male")
        this.gender="male"
      else
        this.gender="female"
      
	  })
  }
  onClickSubmit(userDetails:any){
    var requestURL=this._requestURL.requestURL_user+'upruser'      
    this._http.post(requestURL,userDetails).subscribe((data)=>{
      console.log(data)
      alert("User profile updated successfully.....")
      //this._router.navigate([''])  
      this.ngOnInit() //to refresh
    })
  }
}