import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RequesturlService } from '../requesturl.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  requestURL:string="http://localhost:3000/webapi/contact"
  responseData:any
  constructor(private _http:HttpClient,private _route:Router) { }

  ngOnInit(): void {
  }

  onClickSubmit(userdetails:any)
  {
    this._http.post(this.requestURL,userdetails).subscribe((data)=>{
      this.showResponse(data)                            
    })

  }

  showResponse(data:any)
  {
    if(data.response=='success'){
      alert('Message Sent Successfully. Thank You');
       this._route.navigate(['/'])
    } 
    else
    alert('Message Failed to Send. Try Again Later');   
  }

}
