import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequesturlService } from '../requesturl.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cod',
  templateUrl: './cod.component.html',
  styleUrls: ['./cod.component.css']
})
export class CodComponent implements OnInit {

  userId: any
  buyall: any
  userhistorylist: any
  history:any
  totalamount:number=0
  price:any
  paymentHandler:any = null;
  pDetails:any

  constructor(private route:Router, private _requestURL:RequesturlService, private _http:HttpClient) { }

  requestURL:string=this._requestURL.requestURL_user+'cod'

  ngOnInit(): void {
    this.buyall = []
    this.history=[]

    
    if (!!localStorage.getItem('buyall')) {
      var data = JSON.parse(localStorage.getItem('buyall')!)
      this.buyall = data
      this.buyall.forEach((item: any, index: any) => { 

        if (item._id === this.userId) {
          this.buyall.push(item)
          
        }
        
        //console.log(typeof(this.price))
        //console.log(this.totalamount)
        this.price=parseInt(item.ProductPrice)
        this.totalamount = this.totalamount + this.price
        localStorage.setItem('totalamount',this.totalamount.toString())
        
      })
      
    }
  }
}
